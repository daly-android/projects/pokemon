package com.daly.pokemon.domain.repositories

import com.daly.pokemon.domain.models.PokemonInfoModel
import com.daly.pokemon.domain.models.PokemonListingModel

/**
 * Repository to fetch Pokemons related data
 */
interface PokemonRepository {

    /**
     * Get the list of Pokemons
     * @param page - The page number to fetch
     * @return List<[PokemonListingModel]>
     */
    suspend fun getPokemonsList(page: Int): List<PokemonListingModel>?

    /**
     * Get the details of the given Pokemon
     * @param name - The Pokemon name to get its information
     * @return [PokemonInfoModel]
     */
    suspend fun getPokemonDetails(name: String): PokemonInfoModel?
}
