package com.daly.pokemon.domain.use_cases

import com.daly.pokemon.domain.repositories.PokemonRepository

/**
 * Use case to get information for the given Pokemon name using any [PokemonRepository] implementation provided by dependency injection module
 * @param pokemonRepository - Given implementation of [PokemonRepository]
 */
class GetPokemonDetailsUseCase(
    private val pokemonRepository: PokemonRepository
) {
    suspend operator fun invoke(name: String) = pokemonRepository.getPokemonDetails(name = name)
}
