package com.daly.pokemon.domain.models

data class PokemonInfoModel(
    val name: String?,
    val height: Int?,
    val weight: Int?,
    val categories: List<String>?,
    val stats: List<StatModel>?
) {
    data class StatModel(
        val type: Type,
        val value: Int?
    ) {
        enum class Type {
            HIT_POINTS,
            ATTACK,
            DEFENSE,
            SPEED
        }
    }
}
