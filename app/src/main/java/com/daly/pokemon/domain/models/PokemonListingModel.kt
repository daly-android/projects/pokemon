package com.daly.pokemon.domain.models

data class PokemonListingModel(
    val name: String,
    val imageUrl: String
)
