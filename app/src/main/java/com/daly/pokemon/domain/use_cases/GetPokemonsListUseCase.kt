package com.daly.pokemon.domain.use_cases

import com.daly.pokemon.domain.repositories.PokemonRepository

/**
 * Use case to get Pokemons list using any [PokemonRepository] implementation provided by dependency injection module
 * The used case fetch the list for the given page
 * @param pokemonRepository - Given implementation of [PokemonRepository]
 */
class GetPokemonsListUseCase(
    private val pokemonRepository: PokemonRepository
) {
    suspend operator fun invoke(page: Int) = pokemonRepository.getPokemonsList(page = page)
}
