package com.daly.pokemon.data.remote.dto

import com.squareup.moshi.Json

data class StatDto(
    @Json(name = "name") val name: String?
)
