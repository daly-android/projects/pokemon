package com.daly.pokemon.data.remote.dto

import com.squareup.moshi.Json

data class PokemonListResponseDto(
    @Json(name = "results") val results: List<PokemonListingDto>?
)
