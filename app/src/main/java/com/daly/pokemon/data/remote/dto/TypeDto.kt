package com.daly.pokemon.data.remote.dto

import com.squareup.moshi.Json

data class TypeDto(
    @Json(name = "name") val name: String?
)
