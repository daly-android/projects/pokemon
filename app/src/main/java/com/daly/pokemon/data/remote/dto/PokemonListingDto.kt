package com.daly.pokemon.data.remote.dto

import com.squareup.moshi.Json

private const val IMAGE_URL_PREFIX = "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/"
private const val IMAGE_URL_EXTENSION = ".png"

data class PokemonListingDto(
    @Json(name = "name") val name: String?,
    @Json(name = "url") private val url: String?
) {
    val imageUrl: String
        get() {
            val index = url?.split("/".toRegex())?.dropLast(1)?.last()
            return IMAGE_URL_PREFIX + index + IMAGE_URL_EXTENSION
        }
}
