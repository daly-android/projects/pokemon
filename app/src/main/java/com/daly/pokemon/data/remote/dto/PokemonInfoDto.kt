package com.daly.pokemon.data.remote.dto

import com.squareup.moshi.Json

data class PokemonInfoDto(
    @Json(name = "name") val name: String?,
    @Json(name = "height") val height: Int?,
    @Json(name = "weight") val weight: Int?,
    @Json(name = "types") val types: List<TypeResponseDto>?,
    @Json(name = "stats") val stats: List<StatsResponseDto>?
)
