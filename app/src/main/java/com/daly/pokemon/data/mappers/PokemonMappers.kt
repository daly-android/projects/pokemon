package com.daly.pokemon.data.mappers

import com.daly.pokemon.data.remote.dto.PokemonInfoDto
import com.daly.pokemon.data.remote.dto.PokemonListingDto
import com.daly.pokemon.data.remote.dto.StatDto
import com.daly.pokemon.data.remote.dto.StatsResponseDto
import com.daly.pokemon.domain.models.PokemonInfoModel
import com.daly.pokemon.domain.models.PokemonInfoModel.StatModel
import com.daly.pokemon.domain.models.PokemonListingModel

private fun PokemonListingDto.toDomainModel(): PokemonListingModel? {
    if (this.name == null) return null
    return PokemonListingModel(
        name = this.name.replaceFirstChar { it.uppercase() },
        imageUrl = this.imageUrl
    )
}

@JvmName("PokemonListingDtoListToDomainModel")
fun List<PokemonListingDto>.toDomainModel() = this.mapNotNull { it.toDomainModel() }

fun PokemonInfoDto.toDomainModel() = PokemonInfoModel(
    name = this.name?.replaceFirstChar { it.uppercase() },
    height = this.height,
    weight = this.weight,
    categories = this.types?.mapNotNull { it.type?.name } ?: emptyList(),
    stats = this.stats?.toDomainModel() ?: emptyList()
)

private fun StatsResponseDto.toDomainModel(): StatModel? {
    if (this.stat?.name == null) return null
    val type = this.stat.toDomainModel() ?: return null
    return StatModel(
        type = type,
        value = this.baseStat
    )
}

fun StatDto?.toDomainModel() = when (this?.name) {
    "hp" -> StatModel.Type.HIT_POINTS
    "attack" -> StatModel.Type.ATTACK
    "defense" -> StatModel.Type.DEFENSE
    "speed" -> StatModel.Type.SPEED
    else -> null
}

@JvmName("StatsResponseDtoListToDomainModel")
fun List<StatsResponseDto>.toDomainModel() = this.mapNotNull { it.toDomainModel() }
