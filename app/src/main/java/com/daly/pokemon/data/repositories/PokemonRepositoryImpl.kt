package com.daly.pokemon.data.repositories

import com.daly.pokemon.core.network.ApiResult
import com.daly.pokemon.core.network.handleApi
import com.daly.pokemon.data.mappers.toDomainModel
import com.daly.pokemon.data.remote.PokemonApi
import com.daly.pokemon.domain.models.PokemonInfoModel
import com.daly.pokemon.domain.models.PokemonListingModel
import com.daly.pokemon.domain.repositories.PokemonRepository
import javax.inject.Inject

/**
 * [PokemonRepository] implementation to fetch data from [PokemonApi]
 * @param pokemonApi - The API used to fetch data
 */
class PokemonRepositoryImpl @Inject constructor(
    private val pokemonApi: PokemonApi
) : PokemonRepository {

    private companion object {
        const val PAGING_SIZE = 20
    }

    /**
     * @see [PokemonRepository.getPokemonsList]
     */
    override suspend fun getPokemonsList(page: Int): List<PokemonListingModel>? = when (
        val result = handleApi {
            pokemonApi.fetchPokemons(
                limit = PAGING_SIZE,
                offset = page * PAGING_SIZE
            )
        }
    ) {
        is ApiResult.Error, is ApiResult.Exception -> null
        is ApiResult.Success -> result.data.results?.toDomainModel()
    }

    /**
     * @see [PokemonRepository.getPokemonDetails]
     */
    override suspend fun getPokemonDetails(name: String): PokemonInfoModel? =
        when (val result = handleApi { pokemonApi.fetchPokemonInfo(name = name.lowercase()) }) {
            is ApiResult.Error, is ApiResult.Exception -> null
            is ApiResult.Success -> result.data.toDomainModel()
        }
}
