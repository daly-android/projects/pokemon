package com.daly.pokemon.data.remote.dto

import com.squareup.moshi.Json

data class TypeResponseDto(
    @Json(name = "type") val type: TypeDto?
)
