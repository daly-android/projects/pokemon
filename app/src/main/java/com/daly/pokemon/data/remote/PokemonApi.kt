package com.daly.pokemon.data.remote

import com.daly.pokemon.data.remote.dto.PokemonInfoDto
import com.daly.pokemon.data.remote.dto.PokemonListResponseDto
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface PokemonApi {

    @GET("pokemon")
    suspend fun fetchPokemons(
        @Query("limit") limit: Int = 20,
        @Query("offset") offset: Int = 0
    ): Response<PokemonListResponseDto>

    @GET("pokemon/{name}")
    suspend fun fetchPokemonInfo(
        @Path("name") name: String
    ): Response<PokemonInfoDto>
}
