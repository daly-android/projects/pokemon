package com.daly.pokemon.data.remote.dto

import com.squareup.moshi.Json

data class StatsResponseDto(
    @Json(name = "base_stat") val baseStat: Int?,
    @Json(name = "stat") val stat: StatDto?
)
