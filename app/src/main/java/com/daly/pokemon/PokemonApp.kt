package com.daly.pokemon

import android.app.Application
import coil.ImageLoader
import coil.ImageLoaderFactory
import coil.disk.DiskCache
import coil.memory.MemoryCache
import coil.util.DebugLogger
import dagger.hilt.android.HiltAndroidApp
import timber.log.Timber

const val COIL_MEMORY_CACHE_MAX_SIZE_PERCENT = 0.20
const val COIL_DISK_CACHE_DIRECTORY_NAME = "image_cache"
const val COIL_DISK_CACHE_MAX_SIZE = (5 * 1024 * 1024).toLong() // Cache of 5 MB

@HiltAndroidApp
class PokemonApp : Application(), ImageLoaderFactory {
    override fun onCreate() {
        super.onCreate()

        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
    }

    override fun newImageLoader(): ImageLoader {
        return ImageLoader.Builder(this)
            .memoryCache {
                MemoryCache.Builder(this)
                    .maxSizePercent(COIL_MEMORY_CACHE_MAX_SIZE_PERCENT)
                    .build()
            }
            .diskCache {
                DiskCache.Builder()
                    .directory(cacheDir.resolve(COIL_DISK_CACHE_DIRECTORY_NAME))
                    .maxSizeBytes(COIL_DISK_CACHE_MAX_SIZE)
                    .build()
            }
            .logger(DebugLogger())
            .respectCacheHeaders(false)
            .build()
    }
}
