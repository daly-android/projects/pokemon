package com.daly.pokemon.presentation.pokemon_details

import com.daly.pokemon.domain.models.PokemonInfoModel

interface PokemonDetailsScreenState {
    object Loading : PokemonDetailsScreenState
    data class Success(val pokemon: PokemonInfoModel) : PokemonDetailsScreenState
    object Error : PokemonDetailsScreenState
}
