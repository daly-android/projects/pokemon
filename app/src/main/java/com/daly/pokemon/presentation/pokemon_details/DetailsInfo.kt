package com.daly.pokemon.presentation.pokemon_details

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.daly.pokemon.R
import com.daly.pokemon.domain.models.PokemonInfoModel

@Composable
fun DetailsInfo(
    modifier: Modifier = Modifier,
    pokemonInfo: PokemonInfoModel
) {
    Row(
        modifier = modifier.padding(top = 16.dp),
        horizontalArrangement = Arrangement.spacedBy(22.dp, Alignment.CenterHorizontally)
    ) {
        pokemonInfo.categories?.forEach { category ->
            Text(
                modifier = Modifier
                    .background(
                        color = MaterialTheme.colorScheme.surface,
                        shape = RoundedCornerShape(64.dp)
                    )
                    .padding(horizontal = 36.dp, vertical = 8.dp),
                text = category,
                fontWeight = FontWeight.Bold,
                textAlign = TextAlign.Center,
                color = MaterialTheme.colorScheme.primary,
                maxLines = 1,
                fontSize = 16.sp
            )
        }
    }

    Row(
        modifier = modifier.padding(top = 24.dp),
        horizontalArrangement = Arrangement.SpaceEvenly
    ) {
        PokemonInfoItem(
            title = String.format(stringResource(id = R.string.pokemon_details_weight_format), pokemonInfo.weight?.toFloat()?.div(10) ?: 0),
            content = stringResource(id = R.string.pokemon_details_weight_label)
        )

        PokemonInfoItem(
            title = String.format(stringResource(id = R.string.pokemon_details_height_format), pokemonInfo.height?.toFloat()?.div(10) ?: 0),
            content = stringResource(id = R.string.pokemon_details_height_label)
        )
    }
}
