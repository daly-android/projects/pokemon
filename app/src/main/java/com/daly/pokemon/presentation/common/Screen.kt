package com.daly.pokemon.presentation.common

import androidx.annotation.StringRes
import com.daly.pokemon.R

private const val SCREEN_OVERVIEW = "screen_overview"
private const val SCREEN_DETAILS = "screen_details"
sealed class Screen(val route: String, @StringRes val title: Int) {
    data object OverviewScreen : Screen(route = SCREEN_OVERVIEW, title = R.string.screen_overview)
    data object DetailsScreen : Screen(route = SCREEN_DETAILS, title = R.string.screen_details)

    fun withArgs(vararg args: String): String = buildString {
        append(route)
        args.forEach { arg ->
            append("/$arg")
        }
    }

    companion object {
        fun getScreenClassFromRoute(route: String): Screen = if (route.contains(SCREEN_OVERVIEW)) OverviewScreen else DetailsScreen
    }
}
