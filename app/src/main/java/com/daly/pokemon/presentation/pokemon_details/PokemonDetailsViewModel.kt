package com.daly.pokemon.presentation.pokemon_details

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.daly.pokemon.core.DispatcherService
import com.daly.pokemon.domain.use_cases.GetPokemonDetailsUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class PokemonDetailsViewModel @Inject constructor(
    private val getPokemonDetailsUseCase: GetPokemonDetailsUseCase,
    private val dispatcherService: DispatcherService
) : ViewModel() {

    private val _uiState = MutableStateFlow<PokemonDetailsScreenState>(PokemonDetailsScreenState.Loading)
    val uiState: StateFlow<PokemonDetailsScreenState> = _uiState.stateIn(
        initialValue = PokemonDetailsScreenState.Loading,
        scope = viewModelScope,
        started = SharingStarted.WhileSubscribed(5_000)
    )

    fun load(pokemonName: String) {
        viewModelScope.launch(dispatcherService.io) {
            getPokemonDetailsUseCase.invoke(pokemonName)?.let {
                _uiState.value = PokemonDetailsScreenState.Success(it)
            } ?: run {
                _uiState.value = PokemonDetailsScreenState.Error
            }
        }
    }
}
