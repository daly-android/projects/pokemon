package com.daly.pokemon.presentation.pokemon_details

import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.daly.pokemon.R
import com.daly.pokemon.domain.models.PokemonInfoModel.StatModel

@Composable
fun PokemonStatCard(
    modifier: Modifier = Modifier,
    stat: StatModel
) {
    Column(
        modifier = modifier,
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center
    ) {
        Box(
            modifier = Modifier
                .clip(CircleShape)
                .border(3.dp, MaterialTheme.colorScheme.surface, CircleShape)
                .size(100.dp)

        ) {
            Text(
                modifier = Modifier.align(Alignment.Center),
                text = stat.value.toString(),
                color = MaterialTheme.colorScheme.secondary,
                fontWeight = FontWeight.Bold,
                fontSize = 16.sp
            )
        }
        Spacer(modifier = Modifier.height(12.dp))
        Text(
            text = stat.type.getStatLabelFromType(),
            color = MaterialTheme.colorScheme.secondary,
            fontWeight = FontWeight.Bold,
            fontSize = 18.sp
        )
    }
}

@Composable
private fun StatModel.Type.getStatLabelFromType() = when (this) {
    StatModel.Type.HIT_POINTS -> stringResource(id = R.string.pokemon_details_hip_points)
    StatModel.Type.ATTACK -> stringResource(id = R.string.pokemon_details_attack)
    StatModel.Type.DEFENSE -> stringResource(id = R.string.pokemon_details_defense)
    StatModel.Type.SPEED -> stringResource(id = R.string.pokemon_details_speed)
}
