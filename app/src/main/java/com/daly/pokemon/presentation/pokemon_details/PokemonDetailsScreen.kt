package com.daly.pokemon.presentation.pokemon_details

import androidx.compose.animation.AnimatedVisibilityScope
import androidx.compose.animation.ExperimentalSharedTransitionApi
import androidx.compose.animation.SharedTransitionScope
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.foundation.lazy.grid.items
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import com.daly.pokemon.R
import com.daly.pokemon.presentation.common.Loader

@OptIn(ExperimentalSharedTransitionApi::class)
@Composable
fun SharedTransitionScope.PokemonDetailsScreen(
    animatedVisibilityScope: AnimatedVisibilityScope,
    pokemonName: String,
    pokemonImageUrl: String,
    viewModel: PokemonDetailsViewModel = hiltViewModel()
) {
    val state by viewModel.uiState.collectAsStateWithLifecycle()

    LaunchedEffect(Unit) {
        viewModel.load(pokemonName = pokemonName)
    }

    Column(
        modifier = Modifier
            .fillMaxSize()
            .testTag(stringResource(id = R.string.pokemon_details_test_tag))
    ) {
        DetailsHeader(
            animatedVisibilityScope = animatedVisibilityScope,
            modifier = Modifier.fillMaxWidth(),
            pokemonName = pokemonName,
            pokemonImageUrl = pokemonImageUrl
        )
        when (state) {
            PokemonDetailsScreenState.Error -> {
                Box(
                    modifier = Modifier.fillMaxSize(),
                    contentAlignment = Alignment.Center
                ) {
                    Text(
                        text = stringResource(R.string.pokemon_details_error, pokemonName),
                        color = MaterialTheme.colorScheme.error
                    )
                }
            }

            PokemonDetailsScreenState.Loading -> {
                Loader()
            }

            is PokemonDetailsScreenState.Success -> {
                val pokemon = (state as PokemonDetailsScreenState.Success).pokemon

                DetailsInfo(
                    modifier = Modifier.fillMaxWidth(),
                    pokemonInfo = pokemon
                )

                LazyVerticalGrid(
                    modifier = Modifier
                        .testTag(stringResource(id = R.string.pokemon_details_stats_test_tag))
                        .padding(top = 16.dp),
                    columns = GridCells.Fixed(2),
                    horizontalArrangement = Arrangement.SpaceBetween,
                    verticalArrangement = Arrangement.spacedBy(24.dp)
                ) {
                    items(pokemon.stats ?: emptyList()) {
                        PokemonStatCard(
                            modifier = Modifier.fillMaxSize(),
                            stat = it
                        )
                    }
                }
            }
        }
    }
}
