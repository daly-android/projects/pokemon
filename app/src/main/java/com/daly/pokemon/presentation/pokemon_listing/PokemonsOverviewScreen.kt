package com.daly.pokemon.presentation.pokemon_listing

import android.net.Uri
import androidx.compose.animation.AnimatedVisibilityScope
import androidx.compose.animation.ExperimentalSharedTransitionApi
import androidx.compose.animation.SharedTransitionScope
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.foundation.lazy.grid.itemsIndexed
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import androidx.navigation.NavController
import com.daly.pokemon.R
import com.daly.pokemon.presentation.common.Loader
import com.daly.pokemon.presentation.common.Screen

@OptIn(ExperimentalSharedTransitionApi::class)
@Composable
fun SharedTransitionScope.PokemonsOverviewScreen(
    animatedVisibilityScope: AnimatedVisibilityScope,
    viewModel: PokemonsOverviewViewModel = hiltViewModel(),
    navController: NavController
) {
    val state by viewModel.uiState.collectAsStateWithLifecycle()

    when (state) {
        PokemonsOverviewScreenState.Loading -> {
            Loader()
        }

        PokemonsOverviewScreenState.Error -> {
            Box(
                modifier = Modifier.fillMaxSize(),
                contentAlignment = Alignment.Center
            ) {
                Text(
                    text = stringResource(id = R.string.pokemon_overview_error),
                    color = MaterialTheme.colorScheme.error
                )
            }
        }

        is PokemonsOverviewScreenState.Success -> {
            Box(modifier = Modifier.fillMaxSize()) {
                val threadHold = 7
                LazyVerticalGrid(
                    modifier = Modifier.testTag(stringResource(id = R.string.pokemon_overview_pokemons_list_test_tag)),
                    columns = GridCells.Fixed(2),
                    contentPadding = PaddingValues(6.dp)
                ) {
                    val pokemons = (state as? PokemonsOverviewScreenState.Success)?.pokemons ?: emptyList()
                    itemsIndexed(
                        items = pokemons,
                        key = { _, pokemon -> pokemon.name }
                    ) { index, pokemon ->
                        if ((index + threadHold) >= pokemons.size) {
                            viewModel.fetchNextPokemonList()
                        }

                        PokemonCard(
                            pokemon = pokemon,
                            animatedVisibilityScope = animatedVisibilityScope,
                            modifier = Modifier
                                .padding(6.dp)
                                .fillMaxWidth()
                                .testTag(stringResource(id = R.string.pokemon_overview_pokemon_item_test_tag))
                                .clickable {
                                    navController.navigate(Screen.DetailsScreen.withArgs(pokemon.name, Uri.encode(pokemon.imageUrl)))
                                }
                        )
                    }
                }
            }
        }
    }
}
