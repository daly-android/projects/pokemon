package com.daly.pokemon.presentation.common

import androidx.compose.animation.ExperimentalSharedTransitionApi
import androidx.compose.animation.SharedTransitionLayout
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Scaffold
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.currentBackStackEntryAsState
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import com.daly.pokemon.presentation.common.Screen.Companion.getScreenClassFromRoute
import com.daly.pokemon.presentation.pokemon_details.PokemonDetailsScreen
import com.daly.pokemon.presentation.pokemon_listing.PokemonsOverviewScreen

private const val POKEMON_NAME_ARG = "screen_details_parameter_pokemon_name"
private const val POKEMON_IMAGE_URL_NAME_ARG = "screen_details_parameter_pokemon_image_url"

@OptIn(ExperimentalSharedTransitionApi::class)
@Composable
fun MainNavigation() {
    val navController = rememberNavController()

    // Get current back stack entry
    val backStackEntry by navController.currentBackStackEntryAsState()

    // Get the name of the current screen
    val currentScreen = backStackEntry?.destination?.route ?: Screen.OverviewScreen.route

    Scaffold(
        topBar = {
            AppBar(
                currentScreen = getScreenClassFromRoute(currentScreen),
                canNavigateBack = navController.previousBackStackEntry != null,
                navigateUp = { navController.navigateUp() }
            )
        }
    ) { innerPadding ->
        SharedTransitionLayout {
            NavHost(
                navController = navController,
                startDestination = Screen.OverviewScreen.route,
                modifier = Modifier.padding(innerPadding)
            ) {
                composable(route = Screen.OverviewScreen.route) {
                    PokemonsOverviewScreen(
                        navController = navController,
                        animatedVisibilityScope = this
                    )
                }

                composable(
                    route = Screen.DetailsScreen.route + "/{$POKEMON_NAME_ARG}/{$POKEMON_IMAGE_URL_NAME_ARG}",
                    arguments = listOf(
                        navArgument(POKEMON_NAME_ARG) { type = NavType.StringType },
                        navArgument(POKEMON_IMAGE_URL_NAME_ARG) { type = NavType.StringType }
                    )
                ) { backStackEntry ->
                    PokemonDetailsScreen(
                        animatedVisibilityScope = this,
                        pokemonName = backStackEntry.arguments?.getString(POKEMON_NAME_ARG) ?: "",
                        pokemonImageUrl = backStackEntry.arguments?.getString(POKEMON_IMAGE_URL_NAME_ARG) ?: ""
                    )
                }
            }
        }
    }
}
