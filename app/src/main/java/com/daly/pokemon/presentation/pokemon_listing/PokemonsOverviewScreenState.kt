package com.daly.pokemon.presentation.pokemon_listing

import com.daly.pokemon.domain.models.PokemonListingModel

interface PokemonsOverviewScreenState {
    data object Loading : PokemonsOverviewScreenState
    data class Success(val pokemons: List<PokemonListingModel>) : PokemonsOverviewScreenState
    data object Error : PokemonsOverviewScreenState
}
