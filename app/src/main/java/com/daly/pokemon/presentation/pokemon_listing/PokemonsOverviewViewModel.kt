package com.daly.pokemon.presentation.pokemon_listing

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.daly.pokemon.core.DispatcherService
import com.daly.pokemon.domain.models.PokemonListingModel
import com.daly.pokemon.domain.use_cases.GetPokemonsListUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.launch
import javax.inject.Inject

@OptIn(ExperimentalCoroutinesApi::class)
@HiltViewModel
class PokemonsOverviewViewModel @Inject constructor(
    val getPokemonsListUseCase: GetPokemonsListUseCase,
    dispatcherService: DispatcherService
) : ViewModel() {

    private val pokemonFetchingIndex: MutableStateFlow<Int> = MutableStateFlow(0)
    private var pokemonList: List<PokemonListingModel> = emptyList()

    private val _uiState = MutableStateFlow<PokemonsOverviewScreenState>(PokemonsOverviewScreenState.Loading)
    val uiState: StateFlow<PokemonsOverviewScreenState> = _uiState.stateIn(
        initialValue = PokemonsOverviewScreenState.Loading,
        scope = viewModelScope,
        started = SharingStarted.WhileSubscribed(5_000)
    )

    init {
        viewModelScope.launch(dispatcherService.io) {
            pokemonFetchingIndex.collect {
                val pokemonsListResult = getPokemonsListUseCase(page = it)
                pokemonsListResult?.let { pokemons ->
                    pokemonList += pokemons
                    pokemonList = pokemonList.distinct()
                    _uiState.value = PokemonsOverviewScreenState.Success(pokemons = pokemonList)
                } ?: run {
                    _uiState.value = PokemonsOverviewScreenState.Error
                }
            }
        }
    }

    fun fetchNextPokemonList() {
        pokemonFetchingIndex.value++
    }
}
