package com.daly.pokemon.core.network

import com.squareup.moshi.Moshi
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import timber.log.Timber

/**
 * RetrofitBuilder that provide Retrofit client with given OkHttpClient, interceptors and Moshi adapter.
 */
class RetrofitBuilder(
    private val moshi: Moshi,
    private val okHttpClient: OkHttpClient
) {

    private companion object {
        const val BASE_URL = "https://pokeapi.co/api/v2/"
    }

    fun <T> build(apiClass: Class<T>): T {
        Timber.d("RetrofitBuilder.build")
        val client = okHttpClient.newBuilder()
        return buildRetrofitClient(client.build()).create(apiClass)
    }

    private fun buildRetrofitClient(client: OkHttpClient): Retrofit = Retrofit.Builder()
        .baseUrl(BASE_URL)
        .client(client)
        .addConverterFactory(ScalarsConverterFactory.create()) // convert to primitives types
        .addConverterFactory(MoshiConverterFactory.create(moshi)) // convert JSON to object
        .build()
}
