package com.daly.pokemon.core.theme

import androidx.compose.ui.graphics.Color

val primaryLight = Color(0xFF415F91)
val secondaryLight = Color(0xFF565F71)
val errorLight = Color(0xFFBA1A1A)
val backgroundLight = Color(0xFFF9F9FF)
val surfaceLight = Color(0xFFE2E2E9)

val primaryDark = Color(0xFFAAC7FF)
val secondaryDark = Color(0xFFBEC6DC)
val errorDark = Color(0xFFFFB4AB)
val backgroundDark = Color(0xFF111318)
val surfaceDark = Color(0xFF3E4759)
