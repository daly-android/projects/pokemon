package com.daly.pokemon.core.network

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import com.daly.pokemon.BuildConfig
import okhttp3.Interceptor
import okhttp3.logging.HttpLoggingInterceptor

object OkHttpInterceptors {

    private const val SEVEN_DAYS_CACHE = 60 * 60 * 24 * 7
    private const val HEADER_CACHE_CONTROL = "Cache-Control"
    private const val HEADER_CACHE_CONTROL_VALUE = "public, only-if-cached, max-stale="

    /**
     * Build an [HttpLoggingInterceptor] and define its level according to the [BuildConfig] Debug or Release
     * @return [HttpLoggingInterceptor]
     */
    fun buildHttpLoggingInterceptor() = HttpLoggingInterceptor(logger = OkHttpLogger()).apply {
        level = if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY else HttpLoggingInterceptor.Level.NONE
    }

    /**
     *  If there is no Internet, get the cache that was stored 7 days ago.
     *  If the cache is older than 7 days, then discard it and indicate an error in fetching the response.
     *  The 'max-stale' attribute is responsible for this behavior.
     *  The 'only-if-cached' attribute indicates to not retrieve new data; fetch the cache only instead.
     */
    fun buildCacheInterceptor(context: Context) = Interceptor { chain ->
        var request = chain.request()
        if (!hasNetwork(context)) {
            request = request.newBuilder().header(HEADER_CACHE_CONTROL, "$HEADER_CACHE_CONTROL_VALUE$SEVEN_DAYS_CACHE").build()
        }
        chain.proceed(request)
    }

    /**
     * Check if device has network connection
     */
    private fun hasNetwork(context: Context): Boolean {
        val connectivityManager = context.getSystemService(ConnectivityManager::class.java)

        // Network class represents one of the networks that the device is connected to.
        val activeNetwork = connectivityManager.activeNetwork
        return if (activeNetwork == null) {
            false // if there is no active network, then simply no internet connection.
        } else {
            // NetworkCapabilities object contains information about properties of a network
            val netCapabilities = connectivityManager.getNetworkCapabilities(activeNetwork)
            (
                netCapabilities != null &&
                    // indicates that the network is set up to access the internet
                    netCapabilities.hasCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET) &&
                    // indicates that the network provides actual access to the public internet when it is probed
                    netCapabilities.hasCapability(NetworkCapabilities.NET_CAPABILITY_VALIDATED)
                )
        }
    }
}
