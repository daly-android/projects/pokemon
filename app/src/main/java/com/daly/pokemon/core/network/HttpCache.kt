package com.daly.pokemon.core.network

import android.content.Context
import okhttp3.Cache

class HttpCache(
    private val context: Context
) {

    // Cache of 5 MB
    private val cacheSize = (5 * 1024 * 1024).toLong()

    fun buildHttpCache() = Cache(context.cacheDir, cacheSize)
}
