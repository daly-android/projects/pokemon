package com.daly.pokemon.core.network

sealed interface ApiResult<out T : Any> {
    data class Success<out T : Any>(val data: T) : ApiResult<T>
    data class Error<out T : Any>(val code: Int, val message: String?) : ApiResult<T>
    data class Exception<out T : Any>(val e: Throwable) : ApiResult<T>
}
