package com.daly.pokemon.core.network

import okhttp3.logging.HttpLoggingInterceptor
import timber.log.Timber

/**
 * Define [HttpLoggingInterceptor] logger based on [Timber]
 */
class OkHttpLogger : HttpLoggingInterceptor.Logger {
    override fun log(message: String) {
        Timber.v(message)
    }
}
