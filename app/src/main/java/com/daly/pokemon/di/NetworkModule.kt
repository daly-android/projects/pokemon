package com.daly.pokemon.di

import android.content.Context
import com.daly.pokemon.core.network.HttpCache
import com.daly.pokemon.core.network.OkHttpClientBase
import com.daly.pokemon.core.network.RetrofitBuilder
import com.daly.pokemon.data.remote.PokemonApi
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object NetworkModule {

    @Provides
    @Singleton
    fun provideRetrofitClient(moshi: Moshi, okHttpClient: OkHttpClient): PokemonApi {
        return RetrofitBuilder(moshi, okHttpClient).build(PokemonApi::class.java)
    }

    @Provides
    @Singleton
    fun provideMoshi(): Moshi = Moshi.Builder()
        .addLast(KotlinJsonAdapterFactory())
        .build()

    @Provides
    @Singleton
    fun provideOkHttpClient(httpCache: HttpCache, @ApplicationContext context: Context): OkHttpClient = OkHttpClientBase.buildOkHttpClient(httpCache, context)

    @Provides
    @Singleton
    fun provideHttpCache(@ApplicationContext context: Context): HttpCache = HttpCache(context)
}
