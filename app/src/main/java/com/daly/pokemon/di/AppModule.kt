package com.daly.pokemon.di

import com.daly.pokemon.core.DispatcherService
import com.daly.pokemon.core.implementations.DispatcherServiceImpl
import com.daly.pokemon.domain.repositories.PokemonRepository
import com.daly.pokemon.domain.use_cases.GetPokemonDetailsUseCase
import com.daly.pokemon.domain.use_cases.GetPokemonsListUseCase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    @Provides
    @Singleton
    fun provideGetPokemonsListUseCase(repository: PokemonRepository): GetPokemonsListUseCase {
        return GetPokemonsListUseCase(repository)
    }

    @Provides
    @Singleton
    fun provideGetPokemonDetailsUseCase(repository: PokemonRepository): GetPokemonDetailsUseCase {
        return GetPokemonDetailsUseCase(repository)
    }

    @Provides
    fun provideDispatcherPService(): DispatcherService {
        return DispatcherServiceImpl()
    }
}
