package com.daly.pokemon.data.repositories

import com.daly.pokemon.data.remote.PokemonApi
import com.daly.pokemon.data.remote.dto.PokemonInfoDto
import com.daly.pokemon.data.remote.dto.PokemonListResponseDto
import com.daly.pokemon.data.remote.dto.PokemonListingDto
import com.daly.pokemon.utils.InstantExecutorExtension
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.impl.annotations.MockK
import io.mockk.junit5.MockKExtension
import kotlinx.coroutines.test.runTest
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.Assertions.assertNull
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import retrofit2.Response

@ExtendWith(InstantExecutorExtension::class, MockKExtension::class)
internal class PokemonRepositoryImplTest {

    private companion object {
        const val PAGE = 5
        const val LIMIT = 20
        const val OFFSET = PAGE * LIMIT

        const val HEIGHT = 7
        const val WEIGHT = 69
        const val NAME_1 = "bulbasaur"
        const val NAME_1_CAPITALIZED = "Bulbasaur"
        const val NAME_2 = "ivysaur"
        const val IMAGE_URL_1 = "https://pokeapi.co/api/v2/pokemon/1"
        const val IMAGE_URL_2 = "https://pokeapi.co/api/v2/pokemon/2"

        val pokemonListApiResult = PokemonListResponseDto(
            results = listOf(
                PokemonListingDto(
                    name = NAME_1,
                    url = IMAGE_URL_1
                ),
                PokemonListingDto(
                    name = NAME_2,
                    url = IMAGE_URL_2
                )
            )
        )

        val pokemonInfoDto = PokemonInfoDto(
            name = NAME_1,
            height = HEIGHT,
            weight = WEIGHT,
            types = null,
            stats = null
        )
    }

    @MockK
    private lateinit var pokemonApi: PokemonApi

    private lateinit var repository: PokemonRepositoryImpl

    @BeforeEach
    fun setup() {
        repository = PokemonRepositoryImpl(pokemonApi = pokemonApi)
    }

    @Test
    fun testGetPokemonsListSuccess() = runTest {
        // given
        coEvery { pokemonApi.fetchPokemons(LIMIT, OFFSET) } answers { Response.success(pokemonListApiResult) }

        // when
        val result = repository.getPokemonsList(PAGE)

        // then
        coVerify {
            pokemonApi.fetchPokemons(LIMIT, OFFSET)
        }
        assertNotNull(result)
        with(result!!) {
            assertEquals(2, result.size)
        }
        coVerify {
            pokemonApi.fetchPokemons(LIMIT, OFFSET)
        }
    }

    @Test
    fun testGetPokemonsListFailure() = runTest {
        // given
        coEvery { pokemonApi.fetchPokemons(any(), any()) } answers { Response.error(400, null) }

        // when
        val result = repository.getPokemonsList(PAGE)

        // then
        assertNull(result)
        coVerify {
            pokemonApi.fetchPokemons(LIMIT, OFFSET)
        }
    }

    @Test
    fun testGetPokemonsListError() = runTest {
        // given
        coEvery { pokemonApi.fetchPokemons() } answers { throw RuntimeException() }

        // when
        val result = repository.getPokemonsList(PAGE)

        // then
        assertNull(result)
        coVerify {
            pokemonApi.fetchPokemons(LIMIT, OFFSET)
        }
    }

    @Test
    fun testGetPokemonDetailsSuccess() = runTest {
        // given
        coEvery { pokemonApi.fetchPokemonInfo(NAME_1) } answers { Response.success(pokemonInfoDto) }

        // when
        val result = repository.getPokemonDetails(NAME_1)

        // then
        coVerify {
            pokemonApi.fetchPokemonInfo(NAME_1)
        }
        assertNotNull(result)
        with(result!!) {
            assertEquals(NAME_1_CAPITALIZED, name)
            assertEquals(HEIGHT, height)
            assertEquals(WEIGHT, weight)
            assertTrue(categories!!.isEmpty())
            assertTrue(stats!!.isEmpty())
        }
        coVerify {
            pokemonApi.fetchPokemonInfo(NAME_1)
        }
    }

    @Test
    fun testGetPokemonDetailsFailure() = runTest {
        // given
        coEvery { pokemonApi.fetchPokemonInfo(any()) } answers { Response.error(400, null) }

        // when
        val result = repository.getPokemonDetails(NAME_1)

        // then
        assertNull(result)
        coVerify {
            pokemonApi.fetchPokemonInfo(NAME_1)
        }
    }

    @Test
    fun testGetPokemonDetailsError() = runTest {
        // given
        coEvery { pokemonApi.fetchPokemonInfo(any()) } answers { throw RuntimeException() }

        // when
        val result = repository.getPokemonDetails(NAME_1)

        // then
        assertNull(result)
        coVerify {
            pokemonApi.fetchPokemonInfo(NAME_1)
        }
    }
}
