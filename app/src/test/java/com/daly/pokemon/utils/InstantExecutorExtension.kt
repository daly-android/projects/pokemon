package com.daly.pokemon.utils

import android.annotation.SuppressLint
import android.os.SystemClock
import androidx.arch.core.executor.ArchTaskExecutor
import androidx.arch.core.executor.TaskExecutor
import io.mockk.every
import io.mockk.mockkStatic
import io.mockk.unmockkAll
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.StandardTestDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.jupiter.api.extension.AfterAllCallback
import org.junit.jupiter.api.extension.AfterEachCallback
import org.junit.jupiter.api.extension.BeforeAllCallback
import org.junit.jupiter.api.extension.BeforeEachCallback
import org.junit.jupiter.api.extension.ExtensionContext
import java.util.TimeZone

/**
 * A Junit5 extension to mock core objects, fix clock time and so on
 * Usage : `@ExtendWith(InstantExecutorExtension::class)
 * In each unit test, write a beforeEach method with only your own mock
 * Do not write an afterEach method with the unmockAll method, this one is executed in this extension
 **/
@SuppressLint("RestrictedApi")
class InstantExecutorExtension(
    private val dispatcher: CoroutineDispatcher = StandardTestDispatcher()
) : BeforeEachCallback, BeforeAllCallback, AfterAllCallback, AfterEachCallback {

    companion object {
        private const val GMT_EUROPE_TIMEZONE = "Europe/Paris" // "GMT+2"
        private const val ELAPSED_REAL_TIME = 999999999999
    }

    inner class ArchExecutor : TaskExecutor() {
        override fun executeOnDiskIO(runnable: Runnable) = runnable.run()
        override fun postToMainThread(runnable: Runnable) = runnable.run()
        override fun isMainThread(): Boolean = true
    }

    @OptIn(ExperimentalCoroutinesApi::class)
    override fun beforeAll(context: ExtensionContext?) {
        TimeZone.setDefault(TimeZone.getTimeZone(GMT_EUROPE_TIMEZONE))
        ArchTaskExecutor.getInstance().setDelegate(ArchExecutor())
        Dispatchers.setMain(dispatcher)
    }

    override fun beforeEach(context: ExtensionContext?) {
        mockkStatic(SystemClock::class)
        every { SystemClock.elapsedRealtime() } returns ELAPSED_REAL_TIME
    }

    override fun afterEach(context: ExtensionContext?) {
        unmockkAll()
    }

    @OptIn(ExperimentalCoroutinesApi::class)
    override fun afterAll(context: ExtensionContext?) {
        Dispatchers.resetMain()
        ArchTaskExecutor.getInstance().setDelegate(null)
    }
}
