package com.daly.pokemon.domain.use_cases

import com.daly.pokemon.domain.models.PokemonInfoModel
import com.daly.pokemon.domain.repositories.PokemonRepository
import com.daly.pokemon.utils.InstantExecutorExtension
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.impl.annotations.MockK
import io.mockk.junit5.MockKExtension
import kotlinx.coroutines.test.runTest
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.Assertions.assertNull
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith

@ExtendWith(InstantExecutorExtension::class, MockKExtension::class)
internal class GetPokemonDetailsUseCaseTest {

    private companion object {
        const val NAME = "bulbasaur"
        const val HEIGHT = 7
        const val WEIGHT = 69
        const val CATEGORY = "Poison"
    }

    @MockK
    private lateinit var pokemonRepository: PokemonRepository

    private lateinit var useCase: GetPokemonDetailsUseCase

    @BeforeEach
    fun setup() {
        useCase = GetPokemonDetailsUseCase(pokemonRepository = pokemonRepository)
    }

    @Test
    fun testInvokeSuccess() = runTest {
        // given
        val givenPokemonInfoModel = PokemonInfoModel(
            name = NAME,
            height = HEIGHT,
            weight = WEIGHT,
            categories = listOf(CATEGORY),
            stats = null
        )
        coEvery { pokemonRepository.getPokemonDetails(NAME) } returns givenPokemonInfoModel

        // when
        val result = useCase.invoke(NAME)

        // then
        assertNotNull(result)
        with(result!!) {
            assertEquals(NAME, name)
            assertEquals(HEIGHT, height)
            assertEquals(WEIGHT, weight)
            assertEquals(1, categories!!.size)
            assertNull(stats)
        }
        coVerify {
            pokemonRepository.getPokemonDetails(NAME)
        }
    }

    @Test
    fun testInvokeFailure() = runTest {
        // given
        coEvery { pokemonRepository.getPokemonDetails(any()) } returns null

        // when
        val result = useCase.invoke(NAME)

        // then
        assertNull(result)
        coVerify {
            pokemonRepository.getPokemonDetails(NAME)
        }
    }
}
