package com.daly.pokemon.domain.use_cases

import com.daly.pokemon.domain.models.PokemonListingModel
import com.daly.pokemon.domain.repositories.PokemonRepository
import com.daly.pokemon.utils.InstantExecutorExtension
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.impl.annotations.MockK
import io.mockk.junit5.MockKExtension
import kotlinx.coroutines.test.runTest
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.Assertions.assertNull
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith

@ExtendWith(InstantExecutorExtension::class, MockKExtension::class)
internal class GetPokemonsListUseCaseTest {

    private companion object {
        const val PAGE = 5
        const val NAME_1 = "bulbasaur"
        const val NAME_2 = "ivysaur"
        const val IMAGE_URL_1 = "https://pokeapi.co/api/v2/pokemon/1"
        const val IMAGE_URL_2 = "https://pokeapi.co/api/v2/pokemon/2"
    }

    @MockK
    private lateinit var pokemonRepository: PokemonRepository

    private lateinit var useCase: GetPokemonsListUseCase

    @BeforeEach
    fun setup() {
        useCase = GetPokemonsListUseCase(pokemonRepository = pokemonRepository)
    }

    @Test
    fun testInvokeSuccess() = runTest {
        // given
        val givenListOfPokemonListingModel = listOf(
            PokemonListingModel(
                name = NAME_1,
                imageUrl = IMAGE_URL_1
            ),
            PokemonListingModel(
                name = NAME_2,
                imageUrl = IMAGE_URL_2
            )
        )
        coEvery { pokemonRepository.getPokemonsList(PAGE) } returns givenListOfPokemonListingModel

        // when
        val result = useCase.invoke(PAGE)

        // then
        assertNotNull(result)
        with(result!!) {
            assertEquals(2, this.size)
            with(this[0]) {
                assertEquals(NAME_1, name)
                assertEquals(IMAGE_URL_1, imageUrl)
            }
            with(this[1]) {
                assertEquals(NAME_2, name)
                assertEquals(IMAGE_URL_2, imageUrl)
            }
        }
        coVerify {
            pokemonRepository.getPokemonsList(PAGE)
        }
    }

    @Test
    fun testInvokeFailure() = runTest {
        // given
        coEvery { pokemonRepository.getPokemonsList(any()) } returns null

        // when
        val result = useCase.invoke(PAGE)

        // then
        assertNull(result)
        coVerify {
            pokemonRepository.getPokemonsList(PAGE)
        }
    }
}
