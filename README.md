## Pokemon App


## Overview
This application show Pokemon list and details for each Pokemon.

It contain two screens:

- The first screen show Pokemon list so users can click on a specific Pokemon to go to it's details information.

- The second screen show multiple information for a specific Pokemon such as the name, the height, the weight, some stats and it's image.

## Technical stack :
# - Language : Kotlin
# - UI : Jetpack Compose
# - Architecture : MVVM and clean
# - Dependency injection : Dagger-Hilt
# - Build configuration language : Kotlin DSL
# - Network & API : Retrofit, OkHttp, Moshi
# - API Rest : [Pokeapi](https://pokeapi.co)
# - Image Loading : [Coil Compose](https://coil-kt.github.io/coil/compose)
# - Code inspect and format : [JLLeitschuh Ktlint](https://github.com/JLLeitschuh/ktlint-gradle)
# - Unit testing : Jupiter + Mockk

|                                                                                         |                                                                                     |                                                                                                 |
|:---------------------------------------------------------------------------------------:|:-----------------------------------------------------------------------------------:|:-----------------------------------------------------------------------------------------------:|
|  <img width="1604" alt="pokemons_overview_dark" src="docs/pokemons_overview_dark.png">  |  <img width="1604" alt="pokemon_details_dark" src="docs/pokemon_details_dark.png">  |  <img width="1604" alt="pokemon_details_error_dark" src="docs/pokemon_details_error_dark.png">  |
| <img width="1604" alt="pokemons_overview_light" src="docs/pokemons_overview_light.png"> | <img width="1604" alt="pokemon_details_light" src="docs/pokemon_details_light.png"> | <img width="1604" alt="pokemon_details_error_light" src="docs/pokemon_details_error_light.png"> |



